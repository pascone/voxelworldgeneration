﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

public class BlockCreator : EditorWindow
{
    public string blockName = "Default";
    public Texture topTexture;
    public Texture sidesTexture;
    public Texture bottomTexture;

    private string texturePath = "Assets/Textures & Materials/";
    private string blockPrefabPath = "/Prefabs/Blocks/Default_Block";

    [MenuItem("Window/VoxelWorldGen/Block Creator")]
    public static void ShowWindow()
    {
        GetWindow(typeof(BlockCreator));
    }

    private void OnGUI()
    {
        GUILayout.Label("Name Of The Block", EditorStyles.boldLabel);
        blockName = EditorGUILayout.TextField("Block Name", blockName);
        GUILayout.Label("Textures Of The Block", EditorStyles.boldLabel);
        topTexture = (Texture2D)EditorGUILayout.ObjectField("Top Texture", topTexture, typeof(Texture2D), false);
        sidesTexture = (Texture2D)EditorGUILayout.ObjectField("Sides Texture", sidesTexture, typeof(Texture2D), false);
        bottomTexture = (Texture2D)EditorGUILayout.ObjectField("Bottom Texture", bottomTexture, typeof(Texture2D), false);

        if (GUILayout.Button("Generate Block"))
        {
            GenerateBlock();
        }
    }

    private void GenerateBlock()
    {
        if (topTexture != null && bottomTexture != null && sidesTexture != null)
        {
            //Create the new folder for the new block textures
            Directory.CreateDirectory(texturePath + blockName);

            //Create new materials for each of the textures
            Material topMat = new Material(Shader.Find("Specular"));
            topMat.mainTexture = topTexture;

            Material bottomMat = new Material(Shader.Find("Specular"));
            topMat.mainTexture = bottomTexture;

            Material sidesMat = new Material(Shader.Find("Specular"));
            topMat.mainTexture = sidesTexture;

            AssetDatabase.CreateAsset(topMat, texturePath + blockName + "/" + blockName + "_Top");
            AssetDatabase.CreateAsset(bottomMat, texturePath + blockName + "/" + blockName + "_Bottom");
            AssetDatabase.CreateAsset(sidesMat, texturePath + blockName + "/" + blockName + "_Sides");


            //Create the new prefab
            GameObject newBlock = (GameObject)Resources.Load(blockPrefabPath);

            newBlock.GetComponent<Block>().Initialize(blockName, topMat, sidesMat, bottomMat);
            AssetDatabase.CreateAsset(newBlock, blockPrefabPath + blockName + "_Block");
        }
    }
}
