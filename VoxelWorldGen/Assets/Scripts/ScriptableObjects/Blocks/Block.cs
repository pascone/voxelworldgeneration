﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField] string blockName;
    [SerializeField] private Material topMaterial;
    [SerializeField] private Material sidesMaterial;
    [SerializeField] private Material bottomMaterial;

    public void Initialize(string _name, Material _top, Material _sides, Material _bottom)
    {
        blockName = _name;
        topMaterial = _top;
        sidesMaterial = _sides;
        bottomMaterial = _bottom;

        GetComponent<MeshRenderer>().materials[0] = bottomMaterial;
        GetComponent<MeshRenderer>().materials[1] = sidesMaterial;
        GetComponent<MeshRenderer>().materials[2] = topMaterial;
    }
}
